(function () {
	// The width and height of captured photo. We will set the
	// width to the value defined here, but the height will be
	// calculated based on the aspect ratio of the the input stream.
	var width = 800; // We will scale the photo width to this
	var height = 0; // This will be computed based on the input stream

	// |streaming| indicates whether or not we're currently streaming
	// video from the camera. Obviously, we start at false.
	var streaming = false;

	// The various HTML elements we need to configure or control. These
	// will be set by the startup() function.
	var video = null;
	var canvas = null;
	var photo = null;
	var startbutton = null;

	function startup() {
		video = document.getElementById('video');
		canvas = document.getElementById('canvas');
		photo = document.getElementById('photo');
		takebutton = document.getElementById('take');

		navigator.mediaDevices.enumerateDevices()
			.then(function (devices) {
				console.log('devices = ', devices);
			}).catch(error => {
				console.log(error);
				reject(error);
			});

		navigator.mediaDevices.getUserMedia({video: true, audio: false})
			.then(function (stream) {
				video.srcObject = stream;
				video.play();
			})
			.catch(function (err) {
				console.log("An error occured: " + err);
			});

		video.addEventListener('canplay', function (ev) {
			if (!streaming) {
				height = video.videoHeight / (video.videoWidth / width);
				console.log('videoHeight = ', video.videoHeight);
				console.log('videoWidth = ', video.videoWidth);

				// Firefox currently has a bug where the height can't be read from
				// the video, so we will make assumptions if this happens.
				if (isNaN(height)) {
					height = width / (4 / 3);
				}

				video.setAttribute('width', width);
				video.setAttribute('height', height);
				canvas.setAttribute('width', width);
				canvas.setAttribute('height', height);
				streaming = true;
			}
		}, false);

		takebutton.addEventListener('click', function (ev) {
			takepicture();
			ev.preventDefault();
		}, false);

		clearphoto();
	}

	// Fill the photo with an indication that none has been
	// captured.
	function clearphoto() {
		var context = canvas.getContext('2d');
		context.fillStyle = "#AAA";
		context.fillRect(0, 0, canvas.width, canvas.height);

		var data = canvas.toDataURL('image/png');
		photo.setAttribute('src', data);
	}

	// Capture a photo by fetching the current contents of the video
	// and drawing it into a canvas, then converting that to a PNG
	// format data URL. By drawing it on an offscreen canvas and then
	// drawing that to the screen, we can change its size and/or apply
	// other changes before drawing it.
	function takepicture() {
		var context = canvas.getContext('2d');
		if (width && height) {
			canvas.width = width;
			canvas.height = height;
			context.drawImage(video, 0, 0, width, height);

			var data = canvas.toDataURL('image/png');
			photo.setAttribute('src', data);
		} else {
			clearphoto();
		}
	}

	// Set up our event listener to run the startup process
	// once loading is complete.
	window.addEventListener('load', startup, false);
})();

function getAlert(message) {
	alert(message + '02');
	getCameraList();
}

function getCameraList() {
	var sel = document.querySelector('#selectId');
	console.log('sel = ', sel);
	while (sel.firstChild) {
		sel.removeChild(sel.firstChild);
	}
	//var optionString = '';
	navigator.mediaDevices.enumerateDevices()
		.then(function (devices) {
			console.log('devices = ', devices);
			devices.forEach(function (device) {
				if (device.kind === 'videoinput') {
					console.log(device.kind + ': ' + device.label + ': id = ' + device.deviceId);
					//optionString += '<option value="' + device.deviceId + '">' + device.label + '</option>';

					var opt = document.createElement('option');
					opt.value = device.deviceId;
					opt.innerHTML = device.label;
					sel.appendChild(opt);
				}
			});
			//sel.append(optionString);
		}).catch(error => {
			console.log(error);
			reject(error);
		});
}

function changeCamera(cameraId) {
	console.log('cameraId = ', cameraId);
}

function stopCamera() {
	console.log('camera stop');
	console.log('video = ', video.srcObject);
	video.srcObject.getTracks().forEach(track => {
		track.stop();
	})
}

function startCamera() {
	console.log('camera start');
	
	var sel = document.querySelector('#selectId');
	let selected = Array.from(sel.options)
		.filter(option => option.selected)
		.map(option => option.value);

	console.log('selected = ', selected);

	console.log('selected.id = ', selected[0]);

	//let mediaConstraints = { video : true, audio : false };
	//let mediaConstraints = { video: true, audio: false, exact: selected[0]};
	
	var videoConstraints = {};
	videoConstraints.deviceId = { exact: selected[0] }
	var mediaConstraints = {
		video: videoConstraints,
		audio: false
	};
	
	console.log('mediaConstraints = ', mediaConstraints);
	
	navigator.mediaDevices.getUserMedia(mediaConstraints)
		.then(function (stream) {
			video.srcObject = stream;
			video.play();
		})
		.catch(function (err) {
			console.log("An error occured: " + err);
		});
	
}